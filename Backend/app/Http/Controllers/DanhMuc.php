<?php

namespace App\Http\Controllers;

use App\Models\Danhmuccap2;
use App\Models\Sanpham;
use Illuminate\Database\Eloquent\Casts\Json;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DanhMuc extends Controller
{
    //

    public function index()
    {

        // lấy danh sách theo id thể loại   
        $data = DB::table('danhmuccap2s')->get();

        return response()->json($data);
    }
    public function getCap1()
    {

        // lấy danh sách theo id thể loại   
        $data = DB::table('danhmuccap1s')->get();

        return response()->json($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'danhmuc_cap1_id' => 'required|string',

            // 'description' => 'required|string',
            // 'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            // Thêm các quy tắc kiểm tra khác nếu cần
        ]);

        #
        // return   $request;

        $product = new Danhmuccap2();
        $product->name = $request->name;
        $product->hinhanh = $request->hinhanh;
        $product->img_product = $request->img_product;
        $product->danhmuc_cap1_id = $request->danhmuc_cap1_id;

        // Xử lý upload hình ảnh
        if ($request->hasFile('hinhanh')) {
            // $image = $request->file('hinhanh');
            // $imageName = time().'.'.$image->getClientOriginalExtension();
            // $image->move(public_path('uploads/products'), $imageName);
            // $product->hinhanh = 'uploads/products/'.$imageName;
            $imagePath = $request->file('hinhanh')->store('products', 'public');
            $product->hinhanh = $imagePath;
        }

        if ($request->hasFile('img_product')) {
            // $image = $request->file('hinhanh');
            // $imageName = time().'.'.$image->getClientOriginalExtension();
            // $image->move(public_path('uploads/products'), $imageName);
            // $product->hinhanh = 'uploads/products/'.$imageName;
            $imagePath = $request->file('img_product')->store('products', 'public');
            $product->img_product = $imagePath;
        }

        $duplicateProduct = Danhmuccap2::where('name', $request->name)->first();
        if ($duplicateProduct) {
            return response()->json(['message' => 'Danh mục đã tạo'], 400);
        } else {
            $product->save();
            return response()->json($product, 201);
        }
    }

    public function update(Request $request, $id)
    {


        $product = Danhmuccap2::findOrFail($id);

        $product->update([
            'name' => $request->input('name'),
            'hinhanh' => $request->hasFile('hinhanh') ? $request->file('hinhanh')->store('products', 'public') : $product->hinhanh,
            'danhmuc_cap1_id' => $request->input('danhmuc_cap1_id'),
            'img_product' => $request->hasFile('img_product') ? $request->file('img_product')->store('products', 'public') : $product->img_product,
        ]);

        return response()->json(['message' => 'Danh mục cập nhật thành công'], 201);
    }
    public function delete($id)
    {


        $product = Danhmuccap2::findOrFail($id);
        $product->delete();


        return response()->json(['message' => 'Xóa danh mục thành công'], 201);
    }
    public function getIdProduct($id)
    {
        //    $data = Sanpham::join('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')->get();
        // return response() -> json($data);

        $product = Danhmuccap2::find($id);
        return response()->json($product);
    }


    public function getById($id)
    {
        $productsInCategory = Sanpham::join('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')
            ->where('danhmuccap2s.id', $id)
            ->select('sanphams.*')
            ->get();
        return response()->json($productsInCategory);
    }
}
