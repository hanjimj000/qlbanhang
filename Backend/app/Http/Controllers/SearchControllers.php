<?php

namespace App\Http\Controllers;

use App\Models\Sanpham;
use Illuminate\Http\Request;

class SearchControllers extends Controller
{
    //

    public function index(Request $request){
        $searchQuery = $request->input('key');

        $data  = Sanpham::where('name', 'like', '%' . $searchQuery . '%')
        ->orWhere('description', 'like', '%' . $searchQuery . '%')
        ->get();

        return  $data;
    }
}
