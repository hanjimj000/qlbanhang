<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    //

    // public function index($productId)
    // {
    //     $comments = Comment::with(['user', 'sanpham'])
    // ->where('sanpham_id', $productId)
    // ->get();
    //     return response()->json($comments);
    // }

    public function show($productId)
    {
        $comments = Comment::with(['user', 'sanpham'])
    ->where('sanpham_id', $productId)
    ->get();
        return response()->json($comments);
    }


    public function store(Request $request)
    
    {

        // return $request;
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'sanpham_id' => 'required|exists:sanphams,id',
            'content' => 'required',
            'rate' => 'nullable|integer|min:1|max:5',
        ]);

        $comment = Comment::create($request->all());

        return response()->json($comment, 201);
    }
}
