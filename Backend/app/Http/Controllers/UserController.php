<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\User_profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //

    public function show($idUser){
 
        // Kiểm tra xem người dùng có tồn tại không
        if ($idUser) {
            // Lấy ID của người dùng hiện tại
            // $userWithOrdersAndDetails = DB::table('users')
            // ->join('orders', 'users.id', '=', 'orders.user_id')
            // ->join('orderdetails', 'orders.id', '=', 'orderdetails.order_id')
            // ->leftJoin('user_profiles', 'users.id', '=', 'user_profiles.user_id')
            // ->select('users.*', 'orders.*',  'orderdetails.*','user_profiles.*')
            // ->where('users.id', $idUser)
            // ->first();
            $user = User::with('order.orderdetails','user_profiles')->find($idUser);

            // Trả về ID của người dùng trong response
            return response()->json(['result' => $user]);
        } else {
            // Người dùng không được xác thực, trả về thông báo hoặc mã lỗi tùy thuộc vào yêu cầu của bạn
            return response()->json(['error' => 'Unauthenticated'], 401);
        }

    }

    public function update(Request $request,$id){

    // Lấy thông tin người dùng từ bảng users
    $user = User::findOrFail($id);
   $request->input('name');
   $request->input('address');
   $request->input('phone');

    $user->name = $request->input('name');
    $user->address = $request->input('address');
    $user->phone = $request->input('phone');

    $user->save();


    $userdetail = User_profile::findOrFail($id);
// return  $userdetail;
    $userdetail -> update([
        'user_id' => $id,
        'hinhanh' => $request->hasFile('hinhanh') ? $request->file('hinhanh')->store('products', 'public'): $userdetail->hinhanh,
        
    ]);

    // $userDetail = User_profile::updateOrCreate(
    //     ['user_id' => $id],
    //     ['hinhanh' => null] ,
    
    //     // Bạn có thể cần thêm các trường khác tại đây
    // );
    // $request->hasFile('hinhanh')  ?  $userDetail-> hinhanh =  $request->file('hinhanh')->store('products', 'public'):    $userDetail-> hinhanh = $userDetail-> hinhanh ;

    // if ($request->hasFile('hinhanh')) {
    //     $imagePath = $request->file('hinhanh')->store('products', 'public');
    // }

    // $userdetail->save();

        return response()->json(['message' => 'Thông tin người dùng đã được cập nhật'], 200);
    }
}
