<?php

namespace App\Http\Controllers;

use App\Models\Danhmuccap2;
use App\Models\Orderdetail;
use App\Models\Sanpham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    //
    public function index()
    {
        // $products = Sanpham::with('danhmuccap2.name')->get();


        $products = Sanpham::join('danhmuccap2s', 'danhmuccap2s.id', '=', 'sanphams.id')
            ->select('sanphams.*', 'danhmuccap2s.name as danhmucname')
            ->get();



        return response()->json($products);
    }

    public function filter(Request $request)
    {
        // $products = Sanpham::with('danhmuccap2.name')->get();


        // $filter = $request->all();
        $categoryId = $request->input('danhmuc_cap2_id',[]);
        $priceRange = $request->input('price');
        $star = $request->input('star');
        // Xử lý bộ lọc

        $query = Sanpham::query();
            if (!empty($categoryId)) {
                $query->whereIn('danhmuc_cap2_id', $categoryId);
            }
        $priceRangee = explode('-', $priceRange);
        if ($priceRangee[0].$priceRangee[1] ==='00') {
            // Xử lý khoảng giá
            $priceRange = explode('-', $priceRange);
            $query->whereBetween('price', [1, 10000]);
          
        }else{
            $priceRange = explode('-', $priceRange);
            $query->whereBetween('price', [$priceRange[0], $priceRange[1]]);
        }

        if (!empty($star)) {
            $products = Sanpham::whereHas('comments', function ($query) use ($star) {
                $query->where('rate', $star);
            })->get();


            return $products;
        }

      
    
     
        // if (!empty($categoryId)) {
        //     $query->whereIn('danhmuc_cap2_id', $categoryId);
        // }
        // Thêm các điều kiện lọc khác nếu cần

        // Lấy danh sách sản phẩm
        $products = $query->get();

        return response()->json($products);
    }
    

    public function selling()
    {
        $top5Products = OrderDetail::select(
            'sanpham_id',
            DB::raw('SUM(quantity) as total_quantity')
        )
            ->groupBy('sanpham_id')
            ->orderByDesc('total_quantity')
            ->limit(5)
            ->get();

        // Lấy thông tin chi tiết của sản phẩm top 5
        $top5ProductDetails = Sanpham::whereIn('id', $top5Products->pluck('sanpham_id')->all())->get();


        // Kết hợp thông tin chi tiết vào kết quả trả về
        $result = $top5Products->map(function ($item) use ($top5ProductDetails) {
            $productDetail = $top5ProductDetails->where('id', $item->sanpham_id)->first();
            return [
                'sanpham_id' => $item->sanpham_id,
                'total_quantity' => $item->total_quantity,
                'hinhanh' => $productDetail ? $productDetail->hinhanh : null,
                'price' => $productDetail ? $productDetail->price : null,

                'product_name' => $productDetail ? $productDetail->name : null,
                // Thêm các thông tin khác của sản phẩm cần hiển thị
            ];
        });


        return response()->json(['data' => $result]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'danhmuc_cap2_id' => 'required|string',

            // 'description' => 'required|string',
            // 'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            // Thêm các quy tắc kiểm tra khác nếu cần
        ]);

        #
        // return   $request;

        $product = new Sanpham();
        $product->name = $request->name;
        $product->price = $request->price;

        $product->description = $request->description;
        $product->content = $request->content;
        $product->so_luong = $request->so_luong;
        $product->danhmuc_cap2_id = $request->danhmuc_cap2_id;

        // Xử lý upload hình ảnh
        if ($request->hasFile('hinhanh')) {
            // $image = $request->file('hinhanh');
            // $imageName = time().'.'.$image->getClientOriginalExtension();
            // $image->move(public_path('uploads/products'), $imageName);
            // $product->hinhanh = 'uploads/products/'.$imageName;
            $imagePath = $request->file('hinhanh')->store('products', 'public');
            $product->hinhanh = $imagePath;
        }

        $duplicateProduct = Sanpham::where('name', $request->name)->first();
        if ($duplicateProduct) {
            return response()->json(['message' => 'Sản phẩm đã tạo'], 400);
        } else {
            $product->save();
            return response()->json($product, 201);
        }
    }

    public function update(Request $request, $id)
    {


        $product = Sanpham::findOrFail($id);

        $product->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'price' => $request->input('price'),

            'so_luong' => $request->input('so_luong'),
            'hinhanh' => $request->hasFile('hinhanh') ? $request->file('hinhanh')->store('products', 'public') : $product->hinhanh,
        ]);

        return response()->json(['message' => 'Sản phẩm cập nhật thành công'], 201);
    }

    public function delete($id)
    {


        $product = Sanpham::findOrFail($id);
        $product->delete();


        return response()->json(['message' => 'Xóa sản phẩm thành công'], 201);
    }

    public function related($id)
    {


        $currentProduct = Sanpham::find($id);

        // Lấy sản phẩm liên quan từ cùng một danh mục (có thể làm thêm các điều kiện khác)
        $relatedProducts = Sanpham::where('danhmuc_cap2_id', $currentProduct->danhmuc_cap2_id)
            ->where('id', '!=', $currentProduct->id) // Để loại trừ sản phẩm hiện tại
            ->limit(10) // Giới hạn số sản phẩm hiển thị
            ->get();
        return response()->json(['relatedProducts' => $relatedProducts], 201);
    }


    // get danh sách theo id thể loại
    public function getIdCategory()
    {
        //    $data = Sanpham::join('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')->get();
        // return response() -> json($data);

        $products = Sanpham::select('sanphams.*', 'danhmuccap2s.name as danhmuccap2s_name')
            ->leftJoin('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')
            ->get();

        return response()->json($products);
    }

    public function getIdProduct($id)
    {
        //    $data = Sanpham::join('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')->get();
        // return response() -> json($data);

        $product = Sanpham::find($id);
        return response()->json($product);
    }


    public function getProductCategory($menuId)
    {
        // $products = Sanpham::join('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')
        // ->select('sanphams.id', 'sanphams.name as product_name', 'sanphams.price', 'danhmuccap2s.name as menu_name','danhmuccap2s.img_product')
        // ->where('danhmuc_cap2_id', $menuId)
        // ->get();

        $data = Danhmuccap2::join('danhmuccap1s', 'danhmuccap2s.danhmuc_cap1_id', '=', 'danhmuccap1s.id')
            ->leftJoin('sanphams', 'danhmuccap2s.id', '=', 'sanphams.danhmuc_cap2_id')
            ->select(
                'danhmuccap2s.id',
                'danhmuccap2s.name as danhmuccap2s_name',
                'danhmuccap2s.img_product as img_banner',

                'danhmuccap2s.name as menu_name',
                DB::raw('count(sanphams.id) as count_sanpham')
            )
            ->where('danhmuccap2s.danhmuc_cap1_id', $menuId)
            ->groupBy('danhmuccap2s.id', 'danhmuccap2s.name', 'danhmuccap2s.img_product')
            ->get();

        return response()->json($data);
    }

    public function getProductCategoryCap2($menuId)
    {
        $products = Sanpham::join('danhmuccap2s', 'sanphams.danhmuc_cap2_id', '=', 'danhmuccap2s.id')
            ->select(
                'sanphams.id',
                'sanphams.name as product_name',
                'sanphams.price',
                'danhmuccap2s.name as menu_name',
                'danhmuccap2s.img_product',
                'sanphams.hinhanh'
            )
            ->where('danhmuc_cap2_id', $menuId)
            ->get();


        return response()->json($products);
    }
}
