<?php

namespace App\Http\Controllers;

use App\Models\Cart as ModelsCart;
use Illuminate\Http\Request;
use App\Models\Sanpham;
use Illuminate\Support\Facades\DB; // Import class DB

class Cart extends Controller
{
    //
    public function index()
    {

        $data = DB::table('carts')->get();
        return $data;
    }

    public function update(Request $request)
    {
        //  return  $request ->input('data');
        $data = $request->input('data');

        // Tạo mảng dữ liệu mới để cập nhật
        $updates = [];
        foreach ($data as $itemData) {
            $productId = $itemData['sanpham_id'];
            $quantity = $itemData['quantity'];
            $price = $itemData['price'];

            $user_id = $itemData['user_id'];

            // Thêm dữ liệu mới vào mảng
            ModelsCart::updateOrInsert(
                ['sanpham_id' => $productId, 'user_id' => $user_id],
                [
                    'quantity' => $quantity,
                    'price' => $price,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );
        }

        // Sử dụng transaction để đảm bảo tính nhất quán khi cập nhật nhiều bản ghi


        return response()->json(['message' => 'Cập nhật giỏ hàng thành công'], 200);
    }
    public function delete($id)
    {

        $cart = ModelsCart::findOrFail($id);
        $cart->delete();


        return response()->json(['message' => 'Xóa sản phẩm thành công'], 201);
    }
    public function getUserCart($user_id)
    {
        // $cartItems = ModelsCart::where('user_id', $user_id)->get();
        //         $cartItems = ModelsCart::with('sanpham')->where('user_id', $user_id)->get();
        // $cartCount = ModelsCart::select('sanpham_id')
        // ->selectRaw('COUNT(*) as count')
        // ->groupBy('sanpham_id')
        // ->havingRaw('COUNT(*) > 1')
        // ->get();

        $duplicateProducts = ModelsCart::select('carts.sanpham_id','carts.id', 'sanphams.name', 'carts.quantity', 'sanphams.hinhanh', 'sanphams.price')
            ->join('sanphams', 'carts.sanpham_id', '=', 'sanphams.id')
            ->join('users', 'carts.user_id', '=', 'users.id') // Assuming user_id is the foreign key in the carts table
            ->where('carts.user_id', $user_id)

            ->groupBy('carts.sanpham_id','carts.id', 'sanphams.name', 'carts.quantity', 'sanphams.hinhanh', 'sanphams.price')
            // ->havingRaw('COUNT(*) > 1' and 'COUNT(*) = 1') 
            ->get();

        //     $noDulicateProducts = ModelsCart::select('carts.sanpham_id', 'sanphams.name')
        // ->selectRaw('COUNT(*) as count')
        //     ->join('sanphams', 'carts.sanpham_id', '=', 'sanphams.id')
        //     ->groupBy('carts.sanpham_id', 'sanphams.name')
        //     ->havingRaw('COUNT(*) = 1')
        //     ->get();


        return response()->json(['data' => $duplicateProducts], 200);
    }

    public function addToCart(Request $request)
    {
        // return $request;
        // Logic để thêm sản phẩm vào giỏ hàng
        $user_id  = $request->input('user_id');
        $sanpham_id = $request->input('sanpham_id');
        $quantity = $request->input('quantity');


        // $request->validate([
        //     // 'name' => 'required|string',
        //     'email' => 'required|email|unique:users',
        //     'password' => 'required|string|min:6',
        // ]);

        $cartItem = ModelsCart::where('user_id', $user_id)
            ->where('sanpham_id', $sanpham_id)
            ->first();

        // return  $cartItem ;

        //  $ahu= ModelsCart::find($user_id);
        // return  $ahu ;

        if ($cartItem) {
            // Nếu sản phẩm đã có, tăng số lượng
            $cartItem->quantity += $quantity;
            $cartItem->save();
            // $cartItem->update([
            //     'quantity' => $quantity,
            // ]);

            // return 13;
        } else {
            // Nếu sản phẩm chưa có, thêm mới vào giỏ hàng
            DB::table('carts')->insert([
                'user_id' => $request->user_id,
                'sanpham_id' => $request->sanpham_id,
                'quantity' => $request->quantity,
                'price' => $request->price,
                // 'hinhanh' => $request->hinhanh,
                // 'sanpham_name' => $request->sanpham_name,
                // 'quantity' => $quantity,

            ]);
        }

        // return $cartItem;

        return response()->json('Thêm giỏ hàng thành công');
    }

    public function removeFromCart(Request $request)
    {
        // Logic để xóa sản phẩm khỏi giỏ hàng
    }

    public function updateQuantity(Request $request)
    {
        // Logic để cập nhật số lượng sản phẩm trong giỏ hàng
    }

    public function showCart()
    {
        // Logic để hiển thị giỏ hàng
    }
}
