<?php

namespace App\Http\Controllers;

use App\Mail\OrderShipped;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Orderdetail;
use App\Models\Sanpham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    //
    public function index()
    {
        $orders = Order::with('orderdetails','user' )->latest('created_at') // Sắp xếp theo thời gian tạo giảm dần
        ->get();   
        // $products = Orderdetail::join('orders', 'orders.id', '=', 'orderdetails.id')
        // ->select('orderdetails.*', 'orderdetails.name as danhmucname')
        // ->get();
    
        return response()->json(['orders' => $orders], 200);
    }

    public function approve($id){

        $order = Order::with('user')->findOrFail($id);

    //    return $order;
        if($order){
        $order->status = 'completed'; // Thay đổi thành trạng thái mong muốn
        $order->save();
        Mail::to($order->user->email)->send(new OrderShipped($order));

        return response()->json(['message' => 'Duyệt đơn hàng thành công'],201);
        }else{
            $order->status = 'completed'; // Thay đổi thành trạng thái mong muốn
        $order->save();
              return response()->json(['message' => 'Duyệt đơn hàng thành công'],201);
        }

    }

    public function store(Request $request)
    {
        // Validation (you can add more validation rules based on your requirements)
        $request->validate([
            'user_id' => 'required|exists:users,id',
            // 'sanpham_id' => 'required|array|min:1',
            'sanphams.*.sanpham_id' => 'required|exists:sanphams,id',
            'sanphams.*.quantity' => 'required|integer|min:1',
        ]);

        // Begin a database transaction
        DB::beginTransaction();

        try {
            // Create a new order
            $order = Order::create([
                'user_id' => $request->input('user_id'),
                'total' => $request->input('user_id'),
                // 'order_date' => now(),
                'status' => 'pending',
            ]);

            // Add order items
            foreach ($request->input('products') as $product) {
                Orderdetail::create([
                    'order_id' => $order->id,
                    'sanpham_id' => $product['sanpham_id'],
                    'quantity' => $product['quantity'],
                    'product_name' => $product['name'],
                    'product_description' => $product['name'],

                    'total_price' => $product['quantity'] * Sanpham::find($product['sanpham_id'])->price,
                ]);
            }
            Cart::where('user_id', $request->input('user_id'))->delete();

            // Commit the transaction
            DB::commit();

            return response()->json(['message' => 'Đặt hàng thành công.', 'order_id' => $order->id], 201);
        } catch (\Exception $e) {
            // If an error occurs, rollback the transaction
            DB::rollback();

            // Log the error for debugging purposes
            Log::error($e);

            // Return an error response
            return response()->json(['message' => 'Failed to create the order.'], 500);
        }
    }
}
