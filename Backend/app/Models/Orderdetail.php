<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    use HasFactory;
    protected $fillable = ['order_id', 'sanpham_id', 'product_name', 'product_description', 'quantity', 'total_price'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function sanpham()
    {
        return $this->belongsTo(Sanpham::class);
    }
}
