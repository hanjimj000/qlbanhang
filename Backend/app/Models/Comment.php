<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;


    protected $fillable = ['user_id', 'sanpham_id', 'content', 'rate'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sanpham()
    {
        return $this->belongsTo(Sanpham::class);
    }

}
