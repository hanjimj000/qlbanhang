<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sanpham extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', // Add any other attributes you want to allow for mass assignment
        'price',
        'description',
        'content',
        'so_luong',
        'danhmuc_cap2_id',

        'hinhanh',
        // ... other attributes
    ];

    public function danhmuccap2()
    {
        return $this->belongsTo(Danhmuccap2::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(Orderdetail::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
