<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_profile extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'address',
        'phone',
        'hinhanh', // Thêm trường hinhanh vào fillable
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
