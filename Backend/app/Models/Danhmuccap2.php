<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Danhmuccap2 extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', // Add any other attributes you want to allow for mass assignment
        'hinhanh',
        'img_product',
        'danhmuc_cap1_id',
        // ... other attributes
    ];

    public function danhmuccap1()
    {
        return $this->belongsTo(Danhmuccap1::class);
    }

    public function sanpham()
    {
        return $this->hasMany(Sanpham::class);
    }
}
