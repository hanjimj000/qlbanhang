<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Danhmuccap1 extends Model
{
    use HasFactory;
  
    public function danhmuccap2()
    {
        return $this->hasMany(Danhmuccap2::class);
    }
}
