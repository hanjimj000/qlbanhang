<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Cart;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DanhMuc;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SearchControllers;
use App\Http\Controllers\UserController;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('/user/{user_id}/cart', [Cart::class, 'getUserCart']);


    // Các route yêu cầu xác thực ở đây
    Route::get('accounts/{id}', [UserController::class, 'show']);
    Route::post('accounts/{id}', [UserController::class, 'update']);

Route::get('danh-muc', [DanhMuc::class,'index']);
Route::get('danh-muc-cap-1', [DanhMuc::class,'getCap1']);

Route::post('danh-muc', [DanhMuc::class,'store']);
Route::get('danh-muc-cap-2/{id}', [DanhMuc::class,'getIdProduct']);
Route::post('danh-muc-cap-2/{id}', [DanhMuc::class,'update']);
Route::delete('danh-muc-cap-2/{id}', [DanhMuc::class,'delete']);

Route::get('danh-muc/{id}', [DanhMuc::class,'getById']);





Route::get('san-pham/', [ProductController::class,'index']);
Route::get('san-pham-filter', [ProductController::class,'filter']);

Route::get('san-pham-selling', [ProductController::class,'selling']);

Route::post('san-pham/', [ProductController::class,'store']);
Route::post('san-pham/{id}', [ProductController::class,'update']);
Route::delete('san-pham/{id}', [ProductController::class,'delete']);
Route::get('san-pham/{id}', [ProductController::class,'getIdProduct']);
Route::get('san-pham-by-category', [ProductController::class,'getIdCategory']);
Route::get('san-pham-related/{id}', [ProductController::class,'related']);


Route::get('danh-muc-san-pham/{id}', [ProductController::class,'getProductCategory']);
Route::get('danh-muc-san-pham-con/{idsp}', [ProductController::class,'getProductCategoryCap2']);


Route::get('search', [SearchControllers::class,'index']);


Route::get('cart', [Cart::class,'index']);
Route::post('cart', [Cart::class,'update']);
Route::delete('cart/{id}', [Cart::class,'delete']);
Route::get('cartByUser', [Cart::class,'cartByUser']);
Route::post('addtocart', [Cart::class,'addToCart']);


/**Comments */

Route::get('comments', [CommentController::class,'index']);
Route::get('comments/{id}', [CommentController::class,'show']);
Route::post('comments', [CommentController::class,'store']);


/**Order */
Route::get('orders', [OrderController::class,'index']);
Route::post('orders', [OrderController::class,'store']);
Route::post('update-orders-status/{id}', [OrderController::class,'approve']);


/**Admin */
// Route::get('admin/sanpham', [Cart::class,'index']);
