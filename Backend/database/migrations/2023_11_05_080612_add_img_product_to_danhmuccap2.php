<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('danhmuccap2s', function (Blueprint $table) {
            //
            $table->text('img_product')->after('hinhanh'); // Thêm cột "age" sau cột "name"

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('danhmuccap2', function (Blueprint $table) {
            //
        });
    }
};
