<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orderdetails', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('sanpham_id');
            $table->string('product_name');
            $table->text('product_description');
            $table->integer('quantity');
            $table->decimal('total_price', 8, 2);
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('sanpham_id')->references('id')->on('sanphams');
            $table->foreignId('sanpham_id')->constrained('sanphams');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orderdetails');
    }
};
