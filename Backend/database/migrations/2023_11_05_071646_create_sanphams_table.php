<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sanphams', function (Blueprint $table) {
            $table->id();
            $table -> string('name',100);
            $table -> float('price');
            $table -> text('description');
            $table -> string('content');
            $table -> integer('so_luong');          
            $table -> text('hinhanh');
            $table->unsignedBigInteger('danhmuc_cap2_id'); // Khóa ngoại
            $table->foreign('danhmuc_cap2_id')->references('id')->on('danhmuccap2s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sanphams');
    }
};
