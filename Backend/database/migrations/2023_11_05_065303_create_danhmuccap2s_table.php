<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('danhmuccap2s', function (Blueprint $table) {
            $table->id();
            $table -> string('name',100);
            $table -> text('hinhanh');
            $table->unsignedBigInteger('danhmuc_cap1_id'); // Khóa ngoại
            $table->foreign('danhmuc_cap1_id')->references('id')->on('danhmuccap1s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('danhmuccap2s');
    }
};
