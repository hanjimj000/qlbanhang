<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->string('hinhanh')->nullable()->change();
            // Thêm các thay đổi khác cho các trường khác nếu cần
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->string('hinhanh')->change();
            // Thêm các thay đổi khác cho các trường khác nếu cần
        });
    }
};
