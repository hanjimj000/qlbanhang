import axios from 'axios';
import { useUserStore } from '../stores/User';


const instance = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/', // Thay thế bằng URL của API của bạn

 
});  

instance.interceptors.request.use(
  (config) => {
    const user = localStorage.getItem('token');
    if (user) {
      config.headers.Authorization = `Bearer ${user}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;