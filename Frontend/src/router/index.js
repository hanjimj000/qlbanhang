import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import DetailProduct from '../views/DetailProduct.vue'
// import CategoryView from '../views/CategoryView.vue'
import CartView from '../views/CartView.vue'
import ShopView from '../views/ShopView.vue'

import PayView from '../views/PayView.vue'
import PayCreateView from '../views/PayCreateView.vue'

import SearchView from '../views/SearchView.vue'
import Account from '../views/Account.vue'

import ProductCategory from '../views/ProductCategory.vue'
import CateProductCap2 from '../views/CateProductCap2.vue'
import Admin from '../Admin/Admin.vue'
import Category from '../Admin/views/Category.vue'
import Product from '../Admin/views/Product.vue'
import OrderView from '../Admin/views/OrderView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [

    {
      path: '/',
      name: 'home',
      component: HomeView
    },
 
    // {
    //   path: '/danh-muc',
    //   name: 'Danh mục', 
    //   component: CategoryView
    // },
    {
      path: '/search',
      name: 'Tìm kiếm', 
      component: SearchView
    },
    {
      path: '/san-pham/:id',
      name: 'Sản phẩm',
      component:DetailProduct

    },
    {
      path: '/gio-hang',
      name: 'Giỏ hàng',
      component:CartView

    },

    {
      path: '/cua-hang',
      name: 'Cửa hàng',
      component:ShopView

    },
    {
      path: '/danh-muc-san-pham/:id',
      name: 'Danh Mục sản phẩm',
      component:ProductCategory

    },
    {
      path: '/danh-muc-san-pham/:id/:idsp',
      name: 'Danh Mục sản phẩm con',
      component:CateProductCap2

    },

    {
      path: '/thanh-toan',
      name: 'Thanh toán',
      component:PayView

    },

    {
      path: '/account',
      name: 'Tài khoản',
      component:Account

    },

    {
      path: '/thanh-toan/create',
      name: 'Thanh toán request',
      component:PayCreateView

    },
    // ADmin ---------------------
    {
      path: '/admin',
      name: 'dardboad',
      component: Admin
    },

    {
      path: '/admin/danh-muc',
      name: 'Danh sách danh mục',
      component: Category
    },
  
    {
      path: '/admin/san-pham',
      name: 'Danh sách Sản phẩm',
      component: Product
    },
    {
      path: '/admin/don-hang',
      name: 'Danh sách Đơn hàng',
      component: OrderView
    },
  ]
})

export default router
