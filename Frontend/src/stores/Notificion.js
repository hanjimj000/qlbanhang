import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { ElNotification } from 'element-plus'

export const useNotificationStore = defineStore({
    id: 'notification',
    state: () => ({
      message: '',
      type: '', // Có thể là 'success', 'error',...
      title:''
    }),
    actions: {
      setNotification(message, type,title) {
        this.message = message;
        this.type = type;
        this.title = title;
      },
      clearNotification() {
        this.message = '';
        this.type = '';
        this.title = '';

      },

      
    },
   
       
    

  });