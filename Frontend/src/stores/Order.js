import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { ElNotification } from 'element-plus'
import axios from '../axios'
export const useOrdersStore = defineStore({
    id: 'order',
    state: () => ({
      listOrders: [],
     
       
    
    }),
    actions: {
      async  getOrders(){
            try {
                let res = await axios.get("orders");
                this.listOrders = res.data.orders.map((item,i)=>({...item,['index']:i+1,['is_status']:item.status==='pending'?false:true}));
              } catch (error) {
                console.log(error);
              }
        },
        async  deleteCategory(id){
        try {
                let res = await axios.delete(`danh-muc-cap-2/${id}`);
                // console.log('res',res)
                  if(res.status===201){
                         ElNotification({
                                    title:'Xóa sản phẩm thành công',
                                    // message: 'This is a success message',
                                    type: "success",
                                });
                  }
              } catch (error) {
                console.log(error);
              }
        },
        setRequest(request){
            this.data.request = request
        },

      getCategoryAll(message) {
        this.key = message;    
    },

    defaultItem(){
          this.data.item  = {
            name:'',
            hinhanh:'',
            img_product:'',
            danhmuc_cap1_id:""
        }
    },

   async editCategory(id) {
    this.loading=true;
        try {
            let res=  await axios.get(`danh-muc-cap-2/${id}`)
                if(res.status===200){
                    this.loading=false;
                    this.data.item = res.data
                    
                }
                this.loading=false;
        } catch (error) {
            console.log(error)
            this.loading=false;

        }
    },
     

      
    },
   
       
    

  });