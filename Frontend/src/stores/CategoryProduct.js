import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from '../axios';
export const useProductCategoryStore = defineStore('items', {
       
    state:()=>({
        
        items:[],

         
    }),

    actions: {
        async fetchProductsByCategory(id) {
            try {
                const response = await axios.get(`danh-muc/${id}`);  
                this.items = response.data;
            } catch (error) {
                console.error('Failed to fetch products:', error);

            }
        },
      },


})
