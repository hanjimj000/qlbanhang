import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { ElNotification } from 'element-plus'

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
      crential: null,
      
    }),
    actions: {
      setLogin(data) {
        this.crential = data;
       
      },

      logout(){
        localStorage.clear();
        this.crential=null;

        
      }
      

      
    },
   
       
    

  });