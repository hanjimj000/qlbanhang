import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { ElNotification } from 'element-plus'

export const useCartStore = defineStore({
    id: 'cart',
    state: () => ({
      cart: [],
      loading:false
    }),
    actions: {
      setCart(data) {
        this.cart = data;
        
      },

      addToCartLoad(load){
        this.loading = load
      },

      logout(){
        localStorage.clear();
        this.crential=null
      }
      

      
    },
   
       
    

  });