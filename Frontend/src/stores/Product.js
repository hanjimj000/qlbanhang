import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { ElNotification } from 'element-plus'
import axios from '../axios'
export const useProductStore = defineStore({
    id: 'product',
    state: () => ({
      listProduct: [],
      loading:false,
      data:{
        request:'',
        item:{
            name:'',
            hinhanh:'',
            price:'',
            description:'',
            content:"",
            so_luong:1,
            danhmuc_cap2_id:""
        },         
  }
    }),
    actions: {
      async  getProduct(){
            try {
                let res = await axios.get("san-pham");
                // console.log('res',res)
                this.listProduct = res.data;
              } catch (error) {
                console.log(error);
              }
        },
        async  deleteProduct(id){
        try {
                let res = await axios.delete(`san-pham/${id}`);
                // console.log('res',res)
                  if(res.status===201){
                         ElNotification({
                                    title:'Xóa sản phẩm thành công',
                                    // message: 'This is a success message',
                                    type: "success",
                                });
                  }
              } catch (error) {
                console.log(error);
              }
        },
        setRequest(request){
            this.data.request = request
        },

      getProductAll(message) {
        this.key = message;    
    },

    defaultItem(){
          this.data.item  = {
            name:'',
            hinhanh:'',
            description:'',
            content:'',
            so_luong:'',
            danhmuc_cap2_id:""
        }
    },

   async editProduct(id) {
    this.loading=true;
        try {
            let res=  await axios.get(`san-pham/${id}`)
                if(res.status===200){
                    this.loading=false;
                    this.data.item = res.data
                    
                }
                this.loading=false;
        } catch (error) {
            console.log(error)
            this.loading=false;

        }
    },
     

      
    },
   
       
    

  });